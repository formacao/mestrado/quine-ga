﻿using Bibliotecas;
using Bibliotecas.Base;
using Bibliotecas.Modelos;
using DigitalCircuit.Interfaces;
using QuineVSGA.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuineVSGA
{
    public static class DriverBaseDeDados
    {
        public static void FinalizarComparação(int idComparação)
        {
            using var contexto = new AplicaçãoContexto();
            using var repositórioComparação = new RepositóriosBase<ComparaçãoModelo>(contexto);

            var comparação = repositórioComparação.Dados.Where(c => c.Id == idComparação).First();

            comparação.Fim = DateTime.Now;
            comparação.TempoExecução = comparação.Fim - comparação.Início;

            repositórioComparação.SalvarAlterações();
        }

        public static int CriarComparação(ArgumentosTerminalDAO argumentos)
        {
            var comparação = new ComparaçãoModelo(DateTime.Now, argumentos.MáximoEntradasCircuito, argumentos.MáximoEntradasPorta);

            using var contexto = new AplicaçãoContexto();
            using var repositórioComparação = new RepositóriosBase<ComparaçãoModelo>(contexto);

            repositórioComparação.Dados.Add(comparação);
            repositórioComparação.SalvarAlterações();

            return comparação.Id;
        }

        public static void DeletarLogs(int id)
        {
            using var contexto = new AplicaçãoContexto();
            using var repositórioLogs = new RepositóriosBase<LogModelo>(contexto);

            var logs = repositórioLogs.Dados.Where(l => l.Execução.Id == id).ToList();

            if (logs.Count > 0)
            {
                repositórioLogs.Dados.RemoveRange(logs);
                repositórioLogs.SalvarAlterações();
            }
        }

        public static void RemoverCircuitoIncompleto(int idCircuito)
        {
            using var contexto = new AplicaçãoContexto();
            using var repositórioCircuito = new RepositóriosBase<CircuitosModelo>(contexto);

            var objeto = repositórioCircuito.Dados.Where(c => c.Id == idCircuito).First();

            repositórioCircuito.Dados.Remove(objeto);
            repositórioCircuito.SalvarAlterações();
        }

        public static int CriarCircuitoNoBancoDeDados(IAnswer tabelaVerdade, int id)
        {
            using var contexto = new AplicaçãoContexto();
            using var repositórioCircuito = new RepositóriosBase<CircuitosModelo>(contexto);
            using var repositórioExecução = new RepositóriosBase<ExecuçãoModelo>(contexto);

            var execução = repositórioExecução.Dados.Where(e => e.Id == id).First();
            var objeto = new CircuitosModelo(execução, tabelaVerdade);

            repositórioCircuito.Dados.Add(objeto);
            repositórioCircuito.SalvarAlterações();

            return objeto.Id;
        }

        public static void RemoverExecuçãoIncompleta(int id)
        {
            using var contexto = new AplicaçãoContexto();
            using var repositório = new RepositóriosBase<ExecuçãoModelo>(contexto);

            var objeto = repositório.Dados.Where(e => e.Id == id).First();

            repositório.Dados.Remove(objeto);
            repositório.SalvarAlterações();
        }

        public static void FinalizarExecuçãoNoBancoDeDados(ExecuçãoModelo execução, int id)
        {
            using var contexto = new AplicaçãoContexto();
            using var repositório = new RepositóriosBase<ExecuçãoModelo>(contexto);

            var objeto = repositório.Dados.Where(e => e.Id == id).First();

            objeto.Atualizar(execução);
            repositório.SalvarAlterações();
        }

        public static int CriarExecuçãoNoBancoDeDados(int idComparação, int quantidadeEntradas, int máximoPortas)
        {
            using var contexto = new AplicaçãoContexto();
            using var repositórioExecução = new RepositóriosBase<ExecuçãoModelo>(contexto);
            using var repositórioComparação = new RepositóriosBase<ComparaçãoModelo>(contexto);

            var comparação = repositórioComparação.Dados.Where(c => c.Id == idComparação).First();
            var execução = new ExecuçãoModelo(comparação, quantidadeEntradas, máximoPortas);

            repositórioExecução.Dados.Add(execução);
            repositórioExecução.SalvarAlterações();

            return execução.Id;
        }
    }
}
