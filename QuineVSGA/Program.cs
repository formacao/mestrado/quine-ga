﻿using Bibliotecas;
using Bibliotecas.Base;
using Bibliotecas.Migrations;
using Bibliotecas.Modelos;
using DigitalCircuit.Interfaces;
using GeneticAlgorithm.Interfaces;
using QuineVSGA.DAO;
using QuineVSGA.Geradores;
using System;
using System.Linq;

namespace QuineVSGA
{
    public class Program
    {
        static void Main(params string[] args)
        {
            var argumentos = ArgumentosTerminalDAO.RetornarArgumentos(args);
            var idComparação = DriverBaseDeDados.CriarComparação(argumentos);

            for (int i = 2; i <= argumentos.MáximoEntradasCircuito; i++)
            {
                var máximoPortas = argumentos.RetornarNúmeroAleatórioPortas();
                var id = DriverBaseDeDados.CriarExecuçãoNoBancoDeDados(idComparação, i, máximoPortas);
                var idCircuito = 0;
                try
                {
                    IAnswer tabelaVerdade = TabelaVerdadeGerador.GerarTabelaVerdade(i);
                    idCircuito = DriverBaseDeDados.CriarCircuitoNoBancoDeDados(tabelaVerdade, id);
                    ExecuçãoModelo execução = ExecuçãoGerador.GerarCircuito(tabelaVerdade, i, máximoPortas, id);
                    DriverBaseDeDados.FinalizarExecuçãoNoBancoDeDados(execução, id);
                }
                catch (Exception e)
                {
                    DriverBaseDeDados.DeletarLogs(id);
                    if (idCircuito != 0) DriverBaseDeDados.RemoverCircuitoIncompleto(idCircuito);
                    DriverBaseDeDados.RemoverExecuçãoIncompleta(id);
                    Console.WriteLine($"Erro do tipo {e.GetType()}: {e.Message}");
                    i--; //Voltar e tentar de novo
                }   
            }

            DriverBaseDeDados.FinalizarComparação(idComparação);
        }
    }
}
