﻿using Bibliotecas;
using Bibliotecas.Base;
using Bibliotecas.Interfaces;
using Bibliotecas.Modelos;
using GeneticAlgorithm.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuineVSGA.Exits
{
    public class BaseDeDadosExit : IExit
    {
        private readonly int _idExecução;

        public BaseDeDadosExit(int idExecução)
        {
            _idExecução = idExecução;
        }

        public void AddStep(int generation, int combo, double grade, int faults, DateTime beginning, DateTime current, string champion)
        {
            using var contexto = new AplicaçãoContexto();
            using var repositórioLog = new RepositóriosBase<LogModelo>(contexto);
            using var repositórioExecução = new RepositóriosBase<ExecuçãoModelo>(contexto);

            ExecuçãoModelo execução = repositórioExecução.Dados.Where(e => e.Id == _idExecução).First();
            var log = new LogModelo(execução, generation, combo, grade, faults, beginning, current, champion.ToString().Replace('\n', '|'));

            repositórioLog.Dados.Add(log);
            repositórioLog.SalvarAlterações();
        }


        public void Finish()
        {
            Console.WriteLine("Finished!");
        }
    }
}
