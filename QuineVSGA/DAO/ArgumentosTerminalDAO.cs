﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuineVSGA.DAO
{
    public class ArgumentosTerminalDAO
    {
        private int _máximoEntradasPorta = 2;

        public int MáximoEntradasCircuito { get; set; }

        public int MáximoEntradasPorta
        {
            get => _máximoEntradasPorta;
            set 
            { 
                _máximoEntradasPorta = value; 
            }
        }

        public ArgumentosTerminalDAO(int máximoEntradasCircuito)
        {
            MáximoEntradasCircuito = máximoEntradasCircuito;
        }

        public ArgumentosTerminalDAO(int máximoEntradasCircuito, int máximoEntradasPorta) : this(máximoEntradasCircuito)
        {
            MáximoEntradasPorta = máximoEntradasPorta;
        }


        public static ArgumentosTerminalDAO RetornarArgumentos(string[] args)
        {
            if (args.Length == 0)
            {
                throw new ArgumentNullException($"Informe pelo menos a quantidade máxima de entradas do circuito");
            }

            else
            {
                if (args.Length == 1)
                {
                    return new ArgumentosTerminalDAO(RetornarMáximoEntradasCircuito(args[0]));
                }
                else if(args.Length == 2)
                {
                    return new ArgumentosTerminalDAO(RetornarMáximoEntradasCircuito(args[0]), RetornarMáximoEntradasPortas(args[1]));
                }
                else
                {
                    throw new ArgumentOutOfRangeException($"Muitos argumentos passados.");
                }
            }
        }

        public int RetornarNúmeroAleatórioPortas()
        {
            var random = new Random();
            return (2 + random.Next(_máximoEntradasPorta - 1));
        }

        private static int RetornarMáximoEntradasPortas(string argumento)
        {
            return RetornarArgumento(argumento, 2);
        }

        private static int RetornarMáximoEntradasCircuito(string argumento)
        {
            return RetornarArgumento(argumento, 2);
        }

        private static int RetornarArgumento(string argumento, int valorMínimo)
        {
            var retorno = int.Parse(argumento);

            if(retorno < valorMínimo)
            {
                throw new ArgumentException($"{nameof(MáximoEntradasCircuito)} deve ser maior que {valorMínimo}.");
            }

            return retorno;
        }
    }
}
