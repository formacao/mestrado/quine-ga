﻿using GeneticAlgorithm.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuineVSGA.Helpers
{
    public class StopFlag : IStopFlag
    {
        public bool Evaluate(int generation, int combo, double grade, int faults)
        {
            return (faults == 0 || combo == 500);
        }
    }
}
