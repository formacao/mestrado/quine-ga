﻿using System;
using System.Collections.Generic;
using System.Text;
using Bibliotecas;
using Bibliotecas.Modelos;
using DigitalCircuit.Data_Structures;
using DigitalCircuit.Helpers;
using DigitalCircuit.Interfaces;
using DigitalOptimizer.Interfaces;
using QuineMcCluskey;
using QuineMcCluskey.Interfaces;

namespace QuineVSGA.Geradores
{
    public static class ExecuçãoGerador
    {
        public static ExecuçãoModelo GerarCircuito(IAnswer tabelaVerdade, int númeroEntradas, int limitePortas, int id)
        {
            DateTime inícioQuine = DateTime.Now;
            IDigitalCircuit circuitoQuine = CircuitoGerador.GerarQuineMcCluskey(tabelaVerdade, númeroEntradas, limitePortas);
            DateTime fimQuine = DateTime.Now;

            IOptimizerResultDAO gaDAO = CircuitoGerador.GerarGA(tabelaVerdade, númeroEntradas, limitePortas, id);

            return new ExecuçãoModelo(númeroEntradas, limitePortas, circuitoQuine.NumberOfTransistors, gaDAO.Transistors, circuitoQuine.NumberOfNodes, gaDAO.Nodes, 0, gaDAO.Faults,
                fimQuine - inícioQuine, gaDAO.ExectionTime, inícioQuine, fimQuine, gaDAO.Start, gaDAO.End, circuitoQuine.ToString().Replace('\n', '|'), 
                gaDAO.Circuit.ToString().Replace('\n', '|'), true);
            
        }
    }
}
