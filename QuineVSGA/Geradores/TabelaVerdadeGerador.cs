﻿using DigitalCircuit.Data_Structures;
using DigitalCircuit.Enums;
using DigitalCircuit.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuineVSGA.Geradores
{
    public static class TabelaVerdadeGerador
    {

        public static IAnswer GerarTabelaVerdade(int entradas)
        {
            var quantidadeLinhas = (int)Math.Pow(2, entradas);
            var random = new Random();
            IAnswer retorno = new Answer(quantidadeLinhas);

            for (int i = 0; i < quantidadeLinhas; i++)
            {
                if (random.NextDouble() < 0.4)
                {
                    retorno.AddRow(LogicStatesEnum.True);
                }
                else if(random.NextDouble() < 0.8)
                {
                    retorno.AddRow(LogicStatesEnum.False);
                }
                else
                {
                    retorno.AddRow(LogicStatesEnum.DontCare);
                }
            }

            return retorno;
        }
    }
}
