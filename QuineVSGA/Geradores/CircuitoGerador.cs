﻿using Bibliotecas.Base;
using DigitalCircuit.Data_Structures;
using DigitalCircuit.Exits;
using DigitalCircuit.Helpers;
using DigitalCircuit.Interfaces;
using DigitalOptimizer;
using DigitalOptimizer.DAO;
using DigitalOptimizer.Helpers.GrowthLimitators;
using DigitalOptimizer.Interfaces;
using GeneticAlgorithm.DAO;
using GeneticAlgorithm.Helpers.Stop_Flags;
using Microsoft.EntityFrameworkCore;
using QuineMcCluskey;
using QuineVSGA.Exits;
using QuineVSGA.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuineVSGA.Geradores
{
    public static class CircuitoGerador
    {

        public static IDigitalCircuit GerarQuineMcCluskey(IAnswer tabelaVerdade, int númeroEntradas, int limitePortas)
        {
            IInputGenerator<Input> gerador = new InputGeneratorHelper();
            IInput[] inputs = gerador.GenerateInputs(númeroEntradas);
            var solver = new QuineMcCluskeySolver(númeroEntradas, tabelaVerdade);

            return solver.Run(inputs, limitePortas);
        }

        public static IOptimizerResultDAO GerarGA(IAnswer tabelaVerdade, int númeroEntradas, int limitePortas, int id)
        {
            IInputGenerator<Input> gerador = new InputGeneratorHelper();
            IInput[] inputs = gerador.GenerateInputs(númeroEntradas);
            IOptimizerConfigurationDAO configuration = new OptimizerConfigurationDAO(tabelaVerdade, inputs, limitePortas);

            Optimizer.Rates = new RatesDAO(0.1, 0.9, 1000 * (númeroEntradas - 1), 2);
            Optimizer.Stop = new StopFlag();
            Optimizer.Configuration = configuration;
            Optimizer.GrowthLimitador = new FixedNumberOfNodesGrowthLimitatorHelper(tabelaVerdade.Count, 10);

            IOptimizer ga = new Optimizer();
            return ga.Run(new BaseDeDadosExit(id), 100);
        }
    }
}
