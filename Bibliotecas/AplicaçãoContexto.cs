﻿using Bibliotecas.Modelos;
using Microsoft.EntityFrameworkCore;
using System;

namespace Bibliotecas
{
    public class AplicaçãoContexto : DbContext
    {
        public DbSet<ExecuçãoModelo> Execuções { get; set; }
        public DbSet<CircuitosModelo> Circuitos { get; set; }
        public DbSet<ComparaçãoModelo> Comparações { get; set; }
        public DbSet<LogModelo> Logs { get; set; }

        public AplicaçãoContexto()
        {
        }

        public AplicaçãoContexto(DbContextOptions<AplicaçãoContexto> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ComparaçãoModelo>().HasKey(c => c.Id);

            modelBuilder.Entity<ExecuçãoModelo>().HasKey(e => e.Id);
            modelBuilder.Entity<ExecuçãoModelo>().HasOne(e => e.Comparação);

            modelBuilder.Entity<LogModelo>().HasKey(l => l.Id);
            modelBuilder.Entity<LogModelo>().HasOne(l => l.Execução);

            modelBuilder.Entity<CircuitosModelo>().HasKey(c => c.Id);
            modelBuilder.Entity<CircuitosModelo>().HasOne(c => c.Execução);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

#if DEBUG || PROD
            var servidor = "170.246.105.251";
            var usuário = "ga";
            var senha = "quine123*";
#else
            var servidor = Environment.GetEnvironmentVariable("ServidorMySQL", EnvironmentVariableTarget.Process);
            var usuário = Environment.GetEnvironmentVariable("UsuarioMySQL", EnvironmentVariableTarget.Process);
            var senha = Environment.GetEnvironmentVariable("SenhaMySQL", EnvironmentVariableTarget.Process);
#endif

#if DEBUG

            var baseDeDados = "quinevsga_dev";
#elif PROD
            var baseDeDados = "quinevsga";
#else
            var baseDeDados = Environment.GetEnvironmentVariable("BaseMySQL", EnvironmentVariableTarget.Process);
#endif
            optionsBuilder.UseMySql($"Server={servidor};User Id={usuário};Password={senha};Database={baseDeDados}");
        }
    }
}
