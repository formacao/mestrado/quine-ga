﻿using Bibliotecas.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibliotecas.Modelos
{
    public class ExecuçãoModelo : ModelosBase
    {

        public ComparaçãoModelo Comparação { get; set; }

        public int QuantidadeEntradas { get; set; }
        public int MáximoEntradas { get; set; }

        public int TransistoresQuine { get; set; }
        public int TransistoresGA { get; set; }

        public int NósQuine { get; set; }
        public int NósGA { get; set; }

        public int FalhasQuine { get; set; }
        public int FalhasGA { get; set; }

        public TimeSpan TempoQuine { get; set; }
        public TimeSpan TempoGA { get; set; }

        public DateTime InícioQuine { get; set; }
        public DateTime FimQuine { get; set; }

        public DateTime InícioGA { get; set; }
        public DateTime FimGA { get; set; }

        public string CircuitoQuine { get; set; }
        public string CircuitoGA { get; set; }

        public bool Finalizado { get; set; }

        public ExecuçãoModelo()
        {

        }

        public ExecuçãoModelo(ComparaçãoModelo comparação, int quantidadeEntradas, int máximoPortas)
        {
            Comparação = comparação;
            QuantidadeEntradas = quantidadeEntradas;
            MáximoEntradas = máximoPortas;
            Finalizado = false;
        }

        public ExecuçãoModelo(int quantidadeEntradas, int máximoEntradas, int transistoresQuine, int transistoresGA, int nósQuine,
            int nósGA, int falhasQuine, int falhasGA, TimeSpan tempoQuine, TimeSpan tempoGA, DateTime inícioQuine, DateTime fimQuine, DateTime inícioGA, DateTime fimGA,
            string circuitoQuine, string circuitoGA, bool finalizado)
        {
            QuantidadeEntradas = quantidadeEntradas;
            MáximoEntradas = máximoEntradas;
            TransistoresQuine = transistoresQuine;
            TransistoresGA = transistoresGA;
            NósQuine = nósQuine;
            NósGA = nósGA;
            FalhasQuine = falhasQuine;
            FalhasGA = falhasGA;
            TempoQuine = tempoQuine;
            TempoGA = tempoGA;
            InícioQuine = inícioQuine;
            FimQuine = fimQuine;
            InícioGA = inícioGA;
            FimGA = fimGA;
            CircuitoQuine = circuitoQuine;
            CircuitoGA = circuitoGA;
            Finalizado = finalizado;
        }

        public override void Atualizar(ModelosBase outro)
        {
            ExecuçãoModelo outroModelo = outro as ExecuçãoModelo;

            QuantidadeEntradas = outroModelo.QuantidadeEntradas;
            MáximoEntradas = outroModelo.MáximoEntradas;
            TransistoresQuine = outroModelo.TransistoresQuine;
            TransistoresGA = outroModelo.TransistoresGA;
            NósQuine = outroModelo.NósQuine;
            NósGA = outroModelo.NósGA;
            FalhasQuine = outroModelo.FalhasQuine;
            FalhasGA = outroModelo.FalhasGA;
            TempoQuine = outroModelo.TempoQuine;
            TempoGA = outroModelo.TempoGA;
            InícioQuine = outroModelo.InícioQuine;
            FimQuine = outroModelo.FimQuine;
            InícioGA = outroModelo.InícioGA;
            FimGA = outroModelo.FimGA;
            CircuitoQuine = outroModelo.CircuitoQuine;
            CircuitoGA = outroModelo.CircuitoGA;
            Finalizado = outroModelo.Finalizado;
        }
    }
}
