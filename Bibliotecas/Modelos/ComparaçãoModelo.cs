﻿using Bibliotecas.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibliotecas.Modelos
{
    public class ComparaçãoModelo : ModelosBase
    {

        public DateTime Início { get; set; }
        public DateTime Fim { get; set; }
        public TimeSpan TempoExecução { get; set; }
        public int LimiteSuperiorEntradasCircuito { get; set; }
        public int LimiteSuperiorEntradasPorta { get; set; }

        public ComparaçãoModelo()
        {

        }

        public ComparaçãoModelo(DateTime executadaEm, int limiteSuperiorEntradasCircuito, int limiteSuperiorEntradasPorta)
        {
            Início = executadaEm;
            LimiteSuperiorEntradasCircuito = limiteSuperiorEntradasCircuito;
            LimiteSuperiorEntradasPorta = limiteSuperiorEntradasPorta;
        }

        public override void Atualizar(ModelosBase outro)
        {
            var outroModelo = outro as ComparaçãoModelo;

            LimiteSuperiorEntradasCircuito = outroModelo.LimiteSuperiorEntradasCircuito;
            LimiteSuperiorEntradasPorta = outroModelo.LimiteSuperiorEntradasPorta;
        }
    }
}
