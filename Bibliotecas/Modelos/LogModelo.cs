﻿using Bibliotecas.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibliotecas.Modelos
{
    public class LogModelo : ModelosBase
    {
        public ExecuçãoModelo Execução { get; set; }
        public int Geração { get; set; }
        public int Combo { get; set; }
        public double Nota { get; set; }
        public int Falhas { get; set; }
        public TimeSpan TempoExecução { get; set; }
        public string Campeão { get; set; }

        public LogModelo()
        {

        }

        public LogModelo(ExecuçãoModelo execução, int geração, int combo, double nota, int falhas, DateTime início, DateTime fim, string campeão)
        {
            Execução = execução;
            Geração = geração;
            Combo = combo;
            Nota = nota;
            Falhas = falhas;
            TempoExecução = fim - início;
            Campeão = campeão;
        }

        public override void Atualizar(ModelosBase outro)
        {
            var outroModelo = outro as LogModelo;

            Execução = outroModelo.Execução;
            Geração = outroModelo.Geração;
            Combo = outroModelo.Combo;
            Nota = outroModelo.Nota;
            Falhas = outroModelo.Falhas;
            TempoExecução = outroModelo.TempoExecução;
            Campeão = outroModelo.Campeão;
        }
    }
}
