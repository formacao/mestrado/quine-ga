﻿using Bibliotecas.Base;
using DigitalCircuit.Enums;
using DigitalCircuit.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibliotecas.Modelos
{
    public class CircuitosModelo : ModelosBase
    {

        public ExecuçãoModelo Execução { get; set; }
        public string TabelaVerdade { get; set; }

        public CircuitosModelo()
        {

        }

        public CircuitosModelo(ExecuçãoModelo execução, IAnswer tabelaVerdade)
        {
            Execução = execução;
            TabelaVerdade = ConverterParaString(tabelaVerdade);
        }


        public override void Atualizar(ModelosBase outro)
        {
            var outroModelo = outro as CircuitosModelo;

            Execução = outroModelo.Execução;
            TabelaVerdade = outroModelo.TabelaVerdade;
        }
        private string ConverterParaString(IAnswer tabelaVerdade)
        {
            var retorno = $"{InterpretarLinha(tabelaVerdade[0])}";

            for (int i = 1; i < tabelaVerdade.Count; i++)
            {
                retorno = $"{retorno}|{InterpretarLinha(tabelaVerdade[i])}";
            }
            return retorno;
        }

        private string InterpretarLinha(LogicStatesEnum linha)
        {
            if (linha == LogicStatesEnum.True) return "1";
            else if (linha == LogicStatesEnum.False) return "0";

            return "x";

        }
    }
}
