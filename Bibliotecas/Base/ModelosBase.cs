﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bibliotecas.Base
{
    public abstract class ModelosBase
    {
        public int Id { get; set; }

        public abstract void Atualizar(ModelosBase outro);
    }
}
