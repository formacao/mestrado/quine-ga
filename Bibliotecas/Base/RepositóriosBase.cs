﻿using Bibliotecas.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibliotecas.Base
{
    public class RepositóriosBase<T> : IRepositório<T> where T : ModelosBase
    {
        public DbSet<T> Dados { get; private set; }
        private readonly DbContext _contexto;

        public RepositóriosBase(DbContext contexto)
        {
            _contexto = contexto;
            Dados = _contexto.Set<T>();
        }

        public virtual void Dispose()
        {
            _contexto.Dispose();
        }

        public virtual int SalvarAlterações()
        {
            return _contexto.SaveChanges();
        }
    }
}
