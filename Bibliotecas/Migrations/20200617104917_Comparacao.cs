﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bibliotecas.Migrations
{
    public partial class Comparacao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ComparaçãoId",
                table: "Execuções",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Comparações",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ExecutadaEm = table.Column<DateTime>(nullable: false),
                    LimiteSuperiorEntradasCircuito = table.Column<int>(nullable: false),
                    LimiteSuperiorEntradasPorta = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comparações", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Execuções_ComparaçãoId",
                table: "Execuções",
                column: "ComparaçãoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Execuções_Comparações_ComparaçãoId",
                table: "Execuções",
                column: "ComparaçãoId",
                principalTable: "Comparações",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Execuções_Comparações_ComparaçãoId",
                table: "Execuções");

            migrationBuilder.DropTable(
                name: "Comparações");

            migrationBuilder.DropIndex(
                name: "IX_Execuções_ComparaçãoId",
                table: "Execuções");

            migrationBuilder.DropColumn(
                name: "ComparaçãoId",
                table: "Execuções");
        }
    }
}
