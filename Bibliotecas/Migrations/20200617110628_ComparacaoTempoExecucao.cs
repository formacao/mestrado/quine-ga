﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bibliotecas.Migrations
{
    public partial class ComparacaoTempoExecucao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExecutadaEm",
                table: "Comparações");

            migrationBuilder.AddColumn<DateTime>(
                name: "Fim",
                table: "Comparações",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "Início",
                table: "Comparações",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<TimeSpan>(
                name: "TempoExecução",
                table: "Comparações",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fim",
                table: "Comparações");

            migrationBuilder.DropColumn(
                name: "Início",
                table: "Comparações");

            migrationBuilder.DropColumn(
                name: "TempoExecução",
                table: "Comparações");

            migrationBuilder.AddColumn<DateTime>(
                name: "ExecutadaEm",
                table: "Comparações",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
