﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bibliotecas.Migrations
{
    public partial class InfoCircitos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CircuitoGA",
                table: "Execuções",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CircuitoQuine",
                table: "Execuções",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CircuitoGA",
                table: "Execuções");

            migrationBuilder.DropColumn(
                name: "CircuitoQuine",
                table: "Execuções");
        }
    }
}
