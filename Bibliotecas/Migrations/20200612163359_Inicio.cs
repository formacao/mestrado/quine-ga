﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bibliotecas.Migrations
{
    public partial class Inicio : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Execuções",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    QuantidadeEntradas = table.Column<int>(nullable: false),
                    MáximoEntradas = table.Column<int>(nullable: false),
                    TransistoresQuine = table.Column<int>(nullable: false),
                    TransistoresGA = table.Column<int>(nullable: false),
                    NósQuine = table.Column<int>(nullable: false),
                    NósGA = table.Column<int>(nullable: false),
                    FalhasQuine = table.Column<int>(nullable: false),
                    FalhasGA = table.Column<int>(nullable: false),
                    TempoQuine = table.Column<TimeSpan>(nullable: false),
                    TempoGA = table.Column<TimeSpan>(nullable: false),
                    InícioQuine = table.Column<DateTime>(nullable: false),
                    FimQuine = table.Column<DateTime>(nullable: false),
                    InícioGA = table.Column<DateTime>(nullable: false),
                    FimGA = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Execuções", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Execuções");
        }
    }
}
