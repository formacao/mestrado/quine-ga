﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bibliotecas.Migrations
{
    public partial class Logs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Circuitos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ExecuçãoId = table.Column<int>(nullable: true),
                    TabelaVerdade = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Circuitos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Circuitos_Execuções_ExecuçãoId",
                        column: x => x.ExecuçãoId,
                        principalTable: "Execuções",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Logs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ExecuçãoId = table.Column<int>(nullable: true),
                    Geração = table.Column<int>(nullable: false),
                    Combo = table.Column<int>(nullable: false),
                    Nota = table.Column<double>(nullable: false),
                    Falhas = table.Column<int>(nullable: false),
                    TempoExecução = table.Column<TimeSpan>(nullable: false),
                    Campeão = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Logs_Execuções_ExecuçãoId",
                        column: x => x.ExecuçãoId,
                        principalTable: "Execuções",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Circuitos_ExecuçãoId",
                table: "Circuitos",
                column: "ExecuçãoId");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_ExecuçãoId",
                table: "Logs",
                column: "ExecuçãoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Circuitos");

            migrationBuilder.DropTable(
                name: "Logs");
        }
    }
}
