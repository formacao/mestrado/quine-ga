﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bibliotecas.Migrations
{
    public partial class ExecucaoFinalizada : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Finalizado",
                table: "Execuções",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Finalizado",
                table: "Execuções");
        }
    }
}
