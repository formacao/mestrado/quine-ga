﻿using Bibliotecas.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibliotecas.Interfaces
{
    public interface IRepositório<T> : IDisposable where T: ModelosBase
    {
        DbSet<T> Dados { get; }

        int SalvarAlterações();
    }
}
