﻿using DigitalCircuit.Enums;
using QuineVSGA.Geradores;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace GeradoresTests.TabelaVerdade
{
    public class GerarTabelaVerdade
    {

        [Theory]
        [InlineData(3)]
        public void CheckNumberOfRowsGivenNumberOfInputs(int númeroEntradas)
        {
            var entradas = TabelaVerdadeGerador.GerarTabelaVerdade(númeroEntradas);
            var númeroDeLinhas = (int)Math.Pow(2, númeroEntradas);

            Assert.Equal(númeroDeLinhas, entradas.Count);
        }
    }
}
